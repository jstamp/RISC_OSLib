; Copyright 2022 RISC OS Open Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

; Assumptions:
; * Single-core machine (no barriers)
; * SWP[B]
; * Needs to be atomic with interrupt handlers: Atomic reads & writes are fine, but non-atomic ones require IRQs disabling
; * No halfword load/store support

; There are only a handful of cases where SWP helps us to avoid IRQ locks
; If locks are required, the LCD routines will be used (which don't use SWP,
; either because it's not available or because it's useless, and which do use
; IRQ locks)

; C _kernel_atomic_exchange_1(volatile void* obj, memory_order order, C desired);
      [ :DEF: Need__kernel_atomic_exchange_1_ARMa
        EXPORT  |_kernel_atomic_exchange_1_ARMa|
|_kernel_atomic_exchange_1_ARMa|
        SWPB    a4, a3, [a1]
        MOV     a1, a4
        Return  ,LinkNotStacked
      ]

; C _kernel_atomic_exchange_4(volatile void* obj, memory_order order, C desired);
      [ :DEF: Need__kernel_atomic_exchange_4_ARMa
        EXPORT  |_kernel_atomic_exchange_4_ARMa|
|_kernel_atomic_exchange_4_ARMa|
        SWP     a4, a3, [a1]
        MOV     a1, a4
        Return  ,LinkNotStacked
      ]

; _Bool _kernel_atomic_flag_test_and_set_explicit(volatile atomic_flag *obj, memory_order order);
      [ :DEF: Need__kernel_atomic_flag_test_and_set_explicit_ARMa
        EXPORT  |_kernel_atomic_flag_test_and_set_explicit_ARMa|
|_kernel_atomic_flag_test_and_set_explicit_ARMa|
        ; Custom implementation, because exchange_4 isn't lock-free
        MOV     a3, #1
        SWP     a2, a3, [a1]
        MOV     a1, a2
        Return  ,LinkNotStacked
      ]

        LTORG

        END
