/* Copyright 2022 RISC OS Open Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include "kernel.h"
#include "swis.h"

#include "Global/RISCOS.h"
#include "Global/ModHand.h"
#include "Global/Services.h"
#include "AsmUtils/rminfo.h"

#include "modhdr.h"

#ifdef USE_ROATOMIC
#include "roatomic.h"
#else
#include <stdatomic.h>
#endif

extern void runtests(void);
extern void runbenchmarks(bool csv);

static int mode = 0;

static void *module_pw;

_kernel_oserror *module_initialise(const char *cmd_tail, int podule_base, void *pw)
{
  module_pw = pw;

  return NULL;
}

_kernel_oserror *module_finalise(int fatal, int podule, void *pw)
{
  return NULL;
}

_kernel_oserror *module_command(const char *arg_string, int argc, int cmd_no, void *pw)
{
  /* Run in usermode? */
  bool usermode = strchr(arg_string,'u');
  /* Run in privileged mode? */
  bool privileged = strchr(arg_string,'p');
  /* Test the unsafe version of the routines? (only does something for roatomic) */
#ifdef USE_ROATOMIC
  roatomic_init(strchr(arg_string,'n') != NULL);
#endif
  switch (cmd_no)
  {
  case CMD_AtomicTest:
    if (privileged)
    {
      printf("privileged:\n");
      runtests();
    }
    if (usermode)
    {
      mode = 1;
      return _swix(OS_Module, _INR(0,2), ModHandReason_Enter, Module_Title, arg_string);
    }
    break;
  case CMD_AtomicBench:
    if (privileged)
    {
      printf("privileged:\n");
      runbenchmarks(true);
    }
    if (usermode)
    {
      mode = 2;
      return _swix(OS_Module, _INR(0,2), ModHandReason_Enter, Module_Title, arg_string);
    }
    break;
  }

  return NULL;
}

void module_service(int service, _kernel_swi_regs *r, void *pw)
{
  if ((service == Service_Memory) && (r->r[2] == Image_RO_Base))
  {
    r->r[1] = 0;
  }
  return;
}

int main(int argc,char **argv)
{
  switch (mode)
  {
  case 1:
    printf("user mode:\n");
    runtests();
    break;
  case 2:
    printf("user mode:\n");
    runbenchmarks(true);
    break;
  }
  mode = 0;
  return 0;
}
